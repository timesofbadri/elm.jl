module ELM

	export ExtremeLearningMachine

	export fit!, predict

	# Base code
	include("base.jl")

end
